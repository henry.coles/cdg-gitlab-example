package com.example.moduleb;

import java.util.List;
import java.util.Optional;

public class AClass {

    public Optional<Widget> find(List<String> in) {
        return in.stream()
                .filter(s -> s.startsWith("FAB"))
                .filter(s -> s.startsWith("BADE"))
                .filter(s -> s.startsWith("BAD"))
                .filter(s -> s.startsWith("BAD"))
                .map(Widget::new)
                .findAny();
    }

}
